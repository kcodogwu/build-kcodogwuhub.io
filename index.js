'use strict';

const metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');

metalsmith(__dirname)
.source('./source')
.destination('./build')
.use(layouts({
    engine: 'handlebars',
    directory: 'templates',
    partials: 'templates/partials',
}))
.build(err => { if (err) throw err; });